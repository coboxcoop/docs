# Seed a Folder

---

So you've been sent an `address` to seed someone's `folder`. Lets go through what you need to do. Go to `Settings > Seeders > Seeder`. Once inside your seeder you can then paste the `folder` address. Thats it! You're successfully seeding a folder using CoBox. Congratulations!

