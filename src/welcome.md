<a href="https://cobox.cloud">
<p align="center">
  <img src="./assets/logo/colour.png" alt="logo" width="300"/>
</p>
</a>

---

> “...just as much about technological solutions as it is about the social tissue that we
can nurture-culture, [as a way] to distribute ownership and establish a shared immune
system.”

---

[CoBox](https://cobox.cloud) is an encrypted p2p file system and distributed back-up tool. With CoBox, there are no centralised servers, no data centers, and no surviellance architecture. Its just you and your peers, providing mutual support.

* Want to create a real-time private box for you and your mates to drop in files, share projects and collaborate?
* Want to back-up an extensive archive of videos or photos across several remote devices?
* Scared your rampaging toddler is going to destroy your external hard drive?

Don't worry, CoBox has got you.

Our software creates encrypted private `folders` on your laptop, which seed and synchronise across a distributed network. A CoBox `folder` is a private area accessible across multiple devices - a device can be owned by another person, or it can be another computer you own. Each `folder` contains its own filesystem, and, under the hood, comes with an append-only log which is used to send `folder`-specific messages. For example:

* Declaring information about the current device to other friends (e.g. a name)
* Annotating files with custom metadata
* Sending messages to other friends within the folder, privately or publicly
* Informing other collaborators that you have invited a new peer to collaborate within the `folder`
