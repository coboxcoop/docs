# Summary

- [Welcome](./welcome.md)
- [Getting Started](./getting_started.md)
  - [App Install](./app_install.md)
  - [Folders](./folders.md)
  - [Seeder Install](./seeder_install.md)
  - [Become an Admin](./become_an_admin.md)
  - [Seed a folder](./seed_a_folder.md)
  - [Secure your keys](./secure_your_keys.md)
- [Acknowledgements](./acknowledgements.md)
