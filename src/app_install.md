# Install

---

You install the CoBox App on your laptop or desktop. Using the app you can:

1. Create private folders for you and your friends
2. Generate and accept invite codes for folders
3. Interact with a shared file system

For those running a [seeder](./seeder_install.md), in the app you can also:
4. Tell your seeder to replicate other's folders

## Download

The recommended way to download the app is through a single file binary
distribution version with the one line install command below. The binary
includes a copy of node and CoBox packaged inside a single file, so you just have
to download one file in order to start sharing your files.

```
curl -o- https://cobox.cloud/releases/cobox-v1.0.0-alpha.1/download.sh | bash
```

You can also download with wget instead...

```
wget -qO- https://cobox.cloud/releases/cobox-v1.0.0-alpha.1/download.sh | bash
```

Once the download has completed, to complete the install, simply run the command shown, or once exported to your `PATH`, just run `cobox`.

## First time setup

Now to set up CoBox for the first time, you'll want to type `cobox start`.

```
cobox start
```

This starts CoBox in a background process. It should also pop open the UI in the browser, where you can choose your nickname, create your first folder, and invite some friends. When its running, the app can be found at [http://localhost:9112](http://localhost:9112).

## What happens when I close my laptop?

When you close your laptop, your CoBox app will stop syncronizing data.

To make sure your data is always available across multiple devices, you'll need other people to seed your data. There are several ways you can do this:

1. Ask a seeder to seed your data for you.
2. Invite friends or other computers you own into the folder. As long as they are online, you will synchronise with them.
3. Setup your own seeder and seed your own data and for your friends. Head on over to the [seeder install section](seeder_install.md) to start seeding your and your friends data.

P.S. If you want CoBox to always run in the background when you start your
computer, follow the instructions for `pm2 startup` ([read more](https://pm2.keymetrics.io/docs/usage/startup/)).

P. P. S. Stay tuned for a desktop and mobile application :)

