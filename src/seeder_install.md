# Seeder Install

A CoBox Seeder is the software to backup or 'seed' your files, similar to how BitTorrent seeders increase resilience. More seeders is a good thing.

The CoBox Seeder takes this concept further by introducing encrypted copies of the seeds. CoBox Seeders seed data without holding the encryption keys. That means anyone running a seeder won't be able to read the content of your files on your encrypted file system.

You can configure a stable computer, such as a Raspberry Pi in your home, or a Virtual Private Server somewhere on the Internet, to begin seeding data.

Once installed, the Seeder enables you to:

1. Create a code to invite friends (or your other devices) to be administrators of the Seeder.
2. Start or stop seeding peers encrypted folders (seeds).

## Installation

To install or update cobox-seeder, you should run the install script. To do that, you may either download and run the script manually, or use the following cURL or Wget command:

```shell
curl -o- https://cobox.cloud/releases/cobox-seeder-v1.0.0-alpha.1/download.sh | bash
```

```shell
wget -qO- https://cobox.cloud/releases/cobox-seeder-v1.0.0-alpha.1/download.sh | bash
```

Running either of the above commands downloads a script and runs it. The script downloads a tarball that contains the release binary required by your operating system with additional assets and unpacks the contents to the install directory.

## Troubleshooting on Linux

`cobox-seeder: command not found`: On Linux, after running the install script, if you see no feedback from your terminal after you type command -v cobox-seeder, simply close your current terminal, open a new terminal, and try verifying again.

## Setup 

This will start the seeder in a background process using `pm2`:

```shell
cobox-seeder up
```
## Configuration

Configure the seeder using `$HOME/.coboxrc`

## Keys

Use the following command to print out the keys that you will need to use to
backup data here.

```shell
cobox-seeder keys
```
