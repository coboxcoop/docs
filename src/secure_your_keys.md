# Secure your Keys

---

Finally, its incredibly important to back up your CoBox secret keys. If you lose this, you will no longer appear to other peers to be the same person.

Navigate to `Settings > Manage Keys > Export Secret Keys`. Restoring accounts using this backup will be available in v2 of CoBox.

In the future, we will make it so you can recover your old `CoBox Secret Keys` using a social key recovery mechanism - you'll be able to get your friends to restore your original keys!
