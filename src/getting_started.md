# Getting Started

---

> **DISCLAIMER:** Please note that CoBox is experimental software still in development. We are currently in a peer review and testing phase. Therefore, use this software at your own risk. If you do use it, and/or would like to participate in giving feedback, we have made it possible to participate in providing automatic and manual feedback within the Client App.
>
> _Magma Collective, 09.03.2020_

## Install the App
The App is how most people will navigate CoBox. Similar to Dropbox this opens a folder on your own computer. The big difference with Dropbox is that you can then provide a code to peers running seeders and they will host an encrypted copy of your data. You will know who has your back-ups, and how many there are. You can also invite collaborators to each encrypted `folder`. Follow these instructions to [create your own CoBox folder](./app_install.md)

## Install the Seeder
You can als join the CoBox network by running a seeder, contributing to the cooperative cloud by seeding (hosting) encrypted seeds (backups) of other peoples data for them. Follow the 5 min installation instructions to [set up your own seeder](./seeder_install.md).

