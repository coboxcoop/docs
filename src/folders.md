# Folders

---

Once you have the client app started, you'll be prompted to give yourself a name. This is the name by which you will be known in all your `folder`s. You can change your name at any time, but the `folder`s you create with the old nickname will persist (we'll work on being able to edit historical nicknames in a future release).

You can view a list of all your `folder`s, and you can go into each of them. 

## Create Folders
Click the `+` icon and select the `create a folder` option, give it a name, and hey presto, you now have your very own private `folder`!

## Using Folders
The client app can be used to check folder health, open the folder, delete the folder and invite friends. You can also see how much diskspace is being used.

## Open Folder 
Clicking this option will open the folder using your systems default file browser.

## Folder health
This section contains your `folders` `cobox folder address`. You need to pass this `cobox folder address` to a Seeder admin for them to seed an encrypted copy of your `folder`. 

When you close your laptop, your CoBox app will stop synchronizing data. To make sure your data is always available, it needs more seeders. 

## Invite Collaborators
If you have a friends `cobox key` then you can generate them a code which they can use to join your `folder`. The code is encrypted to their `cobox key` so only they can use the code. The code contains the name of the `folder` and provides the encryption key for the `folder`. This will allow them read files and write files within the `folder`.

Friends **cannot** be removed once added. Removing friends is on our [Roadmap](https://gitlab.com/coboxcoop/readme/-/wikis/Roadmap).

## Accept an Invite
If you have been provided with an invite you can click the `+` icon from the homepage and choose the `Join a folder` option. You can then paste the code. You will now have access to the `folder`.

## Delete
This action will remove the folder data from the machine you are issuing the delete action. However friends may still have copies of the data, and seeders may still retain encrypted copies. 

You will need to type the name of the folder before the delete button appears. This action is not reversable, but you can join the `folder` again if there are other friends who still have the folder.


