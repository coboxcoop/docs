# Acknowledgements

We stand on the shoulders of giants. We've drawn on the amazing work that others have started and continue today. We've been inspired by many other projects, and we've had a lot of help along the way.

## Thanks to:

* [Franz Frando](https://github.com/frando)
* [Karissa McKelvey](https://github.com/okdistribute)
* [Dominic Tarr](https://github.com/dominictarr/)
* [Kira a.k.a. Noffle](https://github.com/noffle/)
* [Alex Cobleigh](https://github.com/cblgh)
* [Mathias Buus](https://github.com/mafintosh/)
* [Andrew Osheroff](https://github.com/andrewosh/)

## Shout outs:

* [Peerfs](https://github.com/okdistribute/peerfs)
* [Cabal](https://cabal.chat)
* [Secure Scuttlebutt](https://scuttlebutt.nz)
* [ARSO](https://github.com/arso-project)
* [DigiDem/Mapeo](https://github.com/digidem)
