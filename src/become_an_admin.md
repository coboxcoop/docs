# Becoming or Adding Admins

---

There are a few ways of becoming an admin for a CoBox seeder.

## 1. During installation

When following the [setup a seeder](./setup_a_seeder) an initial admin is setup.

## 2. Using an Invite Code

Once you have installed the seeder you can generate an invite code to add friends (or your own devices) as an admin for this seeder.

```shell
cobox-seeder invites create <cobox-address> 
```

Doing so will add them as a superuser to this seeder, with the same powers as the original admin. Doing so will mean the seeder can be remote controlled from within the CoBox App.

Once the invite code has been received there are two ways to use the code.

### Using the App UI
The Seeder Admin interface can be accessed in `Settings > Seeders`. Click the `+` icon and then paste the invite code in the corresponding section.

### Using the App CLI
From the app CLI you can issue the following command:

```shell
cobox seeders up --code <code>
```
